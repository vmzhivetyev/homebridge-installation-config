# Homebridge installation and basic config
## Installation
### macOS
\# Assuming you have homebrew installed.
```Shell
sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
brew install node
sudo npm install -g --unsafe-perm=true homebridge
sudo npm install -g --unsafe-perm=true homebridge-yeelight-wifi
```
\# Finally copy the `config.json` to `~/.homebridge/`

### Raspberry PI

Default raspberry settings:
```Config
ssh user: pi
ssh password: raspberry
```

Awesome and simple guide: **[Easy install on Raspberry with autorun using pm2](https://github.com/nfarina/homebridge/wiki/Easy-Install-Raspberry-PI-(With-Start-with-boot))**

## Troubleshooting
### HomeKit says "Couldn't add accessory"

Check that raspberry is connected **EITHER** via Wi-Fi **OR** via Ethernet. Multiple available interfaces just breaks homebridge.

### HomeKit says "Accessory already added"

Solution: generate new MAC address (aka `username`) in homebridge config file.